import React from 'react';
import Logon from './pages/Logon';
import Enzyme, { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() })

test('Match spanshot', () => {
  const wrapper = shallow(<Logon />)

  expect(toJson(wrapper)).toMatchSnapshot();

});