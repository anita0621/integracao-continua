import React from 'react';
import Logon from './pages/Logon';
import renderer from 'react-test-renderer';


test('Link changes the class when hovered', () => {
  const component = renderer.create(
    <Logon />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

});